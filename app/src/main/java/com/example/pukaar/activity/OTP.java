package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pukaar.R;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.aabhasjindal.otptextview.OtpTextView;

public class OTP extends AppCompatActivity {
    @BindView(R.id.verify)
    Button verify_button;
    @BindView(R.id.otp_view)
    OtpTextView otpTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);
        ButterKnife.bind(this);
// Notification Manager

        NotificationManager mNotificationManager;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "notify_001");
        Intent ii = new Intent(getApplicationContext(), sign_up.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, ii, 0);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.setBigContentTitle("Your OTP Is");
        bigText.setSummaryText("OTP");

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.pukaar_logo);
        mBuilder.setContentTitle("Your Title");
        int number1 = random_number1();


        mBuilder.setContentText(""+ number1);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);

        mNotificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

// === Removed some obsoletes
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String channelId = "Your_channel_id";
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        mNotificationManager.notify(0, mBuilder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        }
        otpTextView.setOTP(String.valueOf(number1));
//char[] number = ("" + number1).toCharArray();
    //    otp_view.setText(String.valueOf(number1));








        verify_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    startActivity(new Intent(OTP.this ,Right_therapist.class));
                    overridePendingTransition(0,0);

                }

        });


    }
    private int random_number1(){
        int min = 100000;
        int max = 999999;

        Random r = new Random();
        int i1 =  + r.nextInt(max - min + 1) + min;
        return  i1;
    }




}

