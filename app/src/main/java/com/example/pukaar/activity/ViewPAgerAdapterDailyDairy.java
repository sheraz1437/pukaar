package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.pukaar.response.DiaryResponse;

public class ViewPAgerAdapterDailyDairy extends FragmentPagerAdapter {
    private final DiaryResponse respones;
    int tab_count;

    public ViewPAgerAdapterDailyDairy(@NonNull FragmentManager fm, int behavior, DiaryResponse response) {
        super(fm, behavior);
        tab_count=behavior;
        this.respones = response;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
       switch (position){
           case 0 :
           Day_fragment day_fragment = new Day_fragment();
           day_fragment.setResponse(respones);
               return day_fragment;
           case 1 :   Week_Fragment week_fragment = new Week_Fragment();
               week_fragment.setResponse(respones);
               return week_fragment;
           case 2 : Month_Fragment month_fragment = new Month_Fragment();
           month_fragment.setResponse(respones);
           return month_fragment;
           default: return null;
       }
    }

    @Override
    public int getCount() {
        return tab_count;
    }
}
