package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reuest_1on1_session extends AppCompatActivity {
    @BindView(R.id.back_arrow_12)
    ImageView back_arrow_12;
    @BindView(R.id.one_request_button)
    Button one_request_button;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    String  curDate;
    String  Year;
    String  Month;
    String formattedDate;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reuest1on1_session);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        formattedDate = df.format(c);

        String[] parts = formattedDate.split("-");
        curDate = parts[0];
        Month = parts[1];
        Year = parts[2];

        calendarView.setMinDate(System.currentTimeMillis() - 1000);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                  curDate = String.valueOf(dayOfMonth);
                 Year = String.valueOf(year);
                  Month = String.valueOf(month+1);

               /* Log.e("date",Year+"/"+Month+"/"+curDate);*/
            }
        });


        back_arrow_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Reuest_1on1_session.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        one_request_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<String> call = apiInterface.OneonOneSession(CommonFunction.getToken(getApplicationContext()), curDate+"-"+Month+"-" +Year);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {

                            Toast.makeText(getApplicationContext(), response.body(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(Reuest_1on1_session.this ,Requuest_sent.class));
                            overridePendingTransition(0,0);

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                });
               /* Toast.makeText(getApplicationContext(), curDate+"-"+Month+"-" +Year, Toast.LENGTH_SHORT).show();
               *//* startActivity(new Intent(Reuest_1on1_session.this ,Requuest_sent.class));
                overridePendingTransition(0,0);*/
            }
        });
    }
}