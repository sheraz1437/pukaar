package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Buy_session7 extends AppCompatActivity {

    @BindView(R.id.scanned_button1)
    Button scanned_button1;
    @BindView(R.id.back_arrow_7)
    ImageView back_arrow_7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_session7);
        ButterKnife.bind(this);
        scanned_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Buy_session7.this ,Buy_session4.class));
                overridePendingTransition(0,0);
            }
        });
      back_arrow_7.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              startActivity(new Intent(Buy_session7.this, Buy_session3.class));
              overridePendingTransition(0,0);
          }
      });
    }
}