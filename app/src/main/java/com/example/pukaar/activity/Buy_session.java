package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Buy_session extends AppCompatActivity {
    @BindView(R.id.buy_session_button1)
    Button buy_session_button;
    @BindView(R.id.back_arrow_6)
    ImageView back_arrow_6;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_session);
        ButterKnife.bind(this);
        buy_session_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        back_arrow_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Buy_session.this ,Buy_session7.class));
                overridePendingTransition(0,0);
            }
        });

    }
}