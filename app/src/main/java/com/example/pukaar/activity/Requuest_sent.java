package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Requuest_sent extends AppCompatActivity {
    @BindView(R.id.back_arrow_11)
    ImageView back_arrow_11;
    @BindView(R.id.request_sent_button)
    TextView request_sent_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requuest_sent);

        ButterKnife.bind(this);
        back_arrow_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Requuest_sent.this ,Reuest_1on1_session.class));
                overridePendingTransition(0,0);
            }
        });
        request_sent_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Requuest_sent.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
    }
}