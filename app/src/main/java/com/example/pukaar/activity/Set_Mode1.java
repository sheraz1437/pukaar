package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.SignUpResponse;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.warkiz.widget.Indicator;
import com.warkiz.widget.IndicatorSeekBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Set_Mode1 extends AppCompatActivity {

    @BindView(R.id.bottom_navigation10)
    BottomNavigationView bottomNavigationView10;
    @BindView(R.id.feeling_et)
    EditText feeling_et;
    @BindView(R.id.set_mode1_button1)
    Button set_mode1_button1;

    @BindView(R.id.seekbar1)
    IndicatorSeekBar anxiety;

    @BindView(R.id.seekbar2)
    IndicatorSeekBar energy;

    @BindView(R.id.seekbar3)
    IndicatorSeekBar self;

    private String mood ="";
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_mode1);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mood = getIntent().getStringExtra("mood");
        bottomNavigationView10.getMenu().findItem(R.id.nav_home).setChecked(true);


        bottomNavigationView10.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });

        set_mode1_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (feeling_et.getText().toString().length()<1){
                    Toast.makeText(Set_Mode1.this, "Feeling Minimum 12 word required", Toast.LENGTH_SHORT).show();
                }
                else{
                    Call<String> call = apiInterface.saveDiaryData(CommonFunction.getToken(getApplicationContext()),mood,anxiety.getProgress()+"",energy.getProgress()+"",self.getProgress()+"",feeling_et.getText().toString());
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.body() != null)
                            {
                                if(response.body().toString().toLowerCase().equalsIgnoreCase("Success"))
                                {
                                    Intent intent = new Intent(Set_Mode1.this , Dashboard.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });
                }

            }
        });

    }


}