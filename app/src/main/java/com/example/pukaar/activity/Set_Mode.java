package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;
import com.example.pukaar.common.CommonFunction;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Set_Mode extends AppCompatActivity {

    @BindView(R.id.back_arrow_20)
    ImageView back_arrow_20;
    @BindView(R.id.bottom_navigation9)
    BottomNavigationView bottomNavigationView9;
    @BindView(R.id.set_mode_button1)
    Button Set_mode_button;
    @BindView(R.id.crying_emoji)
    ImageView crying;
    @BindView(R.id.depressed)
    ImageView depressed;
    @BindView(R.id.excited)
    ImageView excited;
    @BindView(R.id.ok)
    ImageView ok;
    @BindView(R.id.sad)
    ImageView sad;
    @BindView(R.id.crying_text)
    TextView crying_text;
    @BindView(R.id.depressed_text)
    TextView depressed_text;
    @BindView(R.id.excited_text)
    TextView excited_text;
    @BindView(R.id.ok_text)
    TextView ok_text;
    @BindView(R.id.sad_text)
    TextView sad_text;
    @BindView(R.id.user_name)
    TextView user_name;
    private CommonFunction commonFunction;
    String seleted_text = "Crying";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_mode);
        ButterKnife.bind(this);
        setImageColor();
        String first_name =commonFunction.getname(this);
        user_name.setText(first_name);

        Set_mode_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Set_Mode.this ,Set_Mode1.class);
                intent.putExtra("mood",seleted_text);
                startActivity(intent);
                overridePendingTransition(0,0);
                finish();
            }
        });
        back_arrow_20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Set_Mode.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        bottomNavigationView9.getMenu().findItem(R.id.nav_home).setChecked(true);


        bottomNavigationView9.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });
    }

    private void setImageColor() {
        crying.setColorFilter(getResources().getColor(R.color.text_blue));
        crying_text.setTextColor(getResources().getColor(R.color.text_blue));
        depressed_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        excited_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        ok_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        sad_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        sad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        depressed.setColorFilter(getResources().getColor(R.color.unselectedColor));
        excited.setColorFilter(getResources().getColor(R.color.unselectedColor));
        ok.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }
    @OnClick(R.id.crying_layout)
    void cryingClick(View view) {
        seleted_text = "Crying";
        crying_text.setTextColor(getResources().getColor(R.color.text_blue));
        depressed_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        excited_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        ok_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        sad_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        crying.setColorFilter(getResources().getColor(R.color.text_blue));
        sad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        depressed.setColorFilter(getResources().getColor(R.color.unselectedColor));
        excited.setColorFilter(getResources().getColor(R.color.unselectedColor));
        ok.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }
    @OnClick(R.id.depressed_layout)
    void depressedclick(View view)
    {
        seleted_text = "Depressed";
        crying_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        depressed_text.setTextColor(getResources().getColor(R.color.text_blue));
        excited_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        ok_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        sad_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        depressed.setColorFilter(getResources().getColor(R.color.text_blue));
        crying.setColorFilter(getResources().getColor(R.color.unselectedColor));
        sad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        excited.setColorFilter(getResources().getColor(R.color.unselectedColor));
        ok.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }
    @OnClick(R.id.excited_layout)
    void excitedclick(View view)
    {
        seleted_text = "Excited";
        crying_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        depressed_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        excited_text.setTextColor(getResources().getColor(R.color.text_blue));
        ok_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        sad_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        excited.setColorFilter(getResources().getColor(R.color.text_blue));
        crying.setColorFilter(getResources().getColor(R.color.unselectedColor));
        depressed.setColorFilter(getResources().getColor(R.color.unselectedColor));
        sad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        ok.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }

    @OnClick(R.id.ok_layout)
    void okclick(View view)
    {
        seleted_text = "Ok";
        crying_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        depressed_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        excited_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        ok_text.setTextColor(getResources().getColor(R.color.text_blue));
        sad_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        ok.setColorFilter(getResources().getColor(R.color.text_blue));
        crying.setColorFilter(getResources().getColor(R.color.unselectedColor));
        depressed.setColorFilter(getResources().getColor(R.color.unselectedColor));
        sad.setColorFilter(getResources().getColor(R.color.unselectedColor));
        excited.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }
    @OnClick(R.id.sad_layout)
    void sadclick(View view)
    {
        seleted_text = "sad";
        crying_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        depressed_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        excited_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        ok_text.setTextColor(getResources().getColor(R.color.unselectedColor));
        sad_text.setTextColor(getResources().getColor(R.color.text_blue));
        sad.setColorFilter(getResources().getColor(R.color.text_blue));
        crying.setColorFilter(getResources().getColor(R.color.unselectedColor));
        depressed.setColorFilter(getResources().getColor(R.color.unselectedColor));
        ok.setColorFilter(getResources().getColor(R.color.unselectedColor));
        excited.setColorFilter(getResources().getColor(R.color.unselectedColor));
    }

}