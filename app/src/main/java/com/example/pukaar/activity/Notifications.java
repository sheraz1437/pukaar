package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.example.pukaar.R;
import com.example.pukaar.adapter.Forum_Adapter;
import com.example.pukaar.adapter.Notification_Adapter;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.ForumResponse;
import com.example.pukaar.response.NotificationResponse;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notifications extends AppCompatActivity {
    @BindView(R.id.back_arrow_13)
    ImageView back_arrow_13;
    @BindView(R.id.bottom_navigation1)
    BottomNavigationView bottomNavigationView;
    private APIInterface apiInterface;
    public Notification_Adapter notification_adapter;
    public NotificationResponse notificationResponse;
    RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        recyclerView = findViewById(R.id.notification_recycler);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ArrayList<NotificationResponse>> call = apiInterface.getNotification(CommonFunction.getToken(getApplicationContext()));
        call.enqueue(new Callback<ArrayList<NotificationResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<NotificationResponse>> call, Response<ArrayList<NotificationResponse>> response) {


                notification_adapter = new Notification_Adapter(getApplicationContext(),response.body() );
                //recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                LinearLayoutManager llm = new LinearLayoutManager(Notifications.this);
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(llm);
                recyclerView.setAdapter(notification_adapter);
            }

            @Override
            public void onFailure(Call<ArrayList<NotificationResponse>> call, Throwable t) {
            }
        });
        back_arrow_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Notifications.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
        bottomNavigationView.getMenu().findItem(R.id.nav_home).setChecked(true);


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });

    }
}