package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.pukaar.R;
import com.example.pukaar.common.CommonFunction;

public class Splash_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(CommonFunction.getToken(getApplicationContext()).length()>10) {
                    Intent intent = new Intent(Splash_screen.this, Dashboard.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }else
                {
                    Intent intent = new Intent(Splash_screen.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }


            }
        },3000);
    }
}