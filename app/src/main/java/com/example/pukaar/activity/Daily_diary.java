package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.DiaryResponse;
import com.example.pukaar.response.SignUpResponse;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Daily_diary extends AppCompatActivity {

    @BindView(R.id.back_arrow_011)
    ImageView back_arrow_011;
    @BindView(R.id.bottom_navigation12)
    BottomNavigationView bottomNavigationView12;

   @BindView(R.id.dwm1)
   TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    ViewPAgerAdapterDailyDairy viewPAgerAdapterDailyDairy;

    TabItem tabItem1;

    TabItem tabItem2;

    TabItem tabItem3;
    private APIInterface apiInterface;
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_diary);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);


        back_arrow_011.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        bottomNavigationView12.getMenu().findItem(R.id.nav_home).setChecked(true);

        setupDiaryData();
        bottomNavigationView12.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });


        tabItem1=findViewById(R.id.tab_item1);
        tabItem2=findViewById(R.id.tab_item2);
        tabItem3=findViewById(R.id.tab_item3);



        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void setupDiaryData() {
        Date date = new Date();
        String enddate= dateFormat.format(new Date(date.getTime() +  1 * 24 * 60 * 60 * 1000));
        Date newDate = new Date(date.getTime() - 604800000L); // 7 * 24 * 60 * 60 * 1000
        String start_date=dateFormat.format(newDate);

        Call<DiaryResponse> call = apiInterface.getDiaryData(CommonFunction.getToken(getApplicationContext()) , start_date , enddate , "10" );
        call.enqueue(new Callback<DiaryResponse>() {
            @Override
            public void onResponse(Call<DiaryResponse> call, Response<DiaryResponse> response) {
                if(response.body()!= null)
                {
                   //Toast.makeText(getApplicationContext(),response.body().data.size()+"",Toast.LENGTH_SHORT);

                    viewPAgerAdapterDailyDairy = new ViewPAgerAdapterDailyDairy(getSupportFragmentManager(),tabLayout.getTabCount(),response.body());
                    viewPager.setAdapter(viewPAgerAdapterDailyDairy);
                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            viewPager.setCurrentItem(tab.getPosition());
                            if (tab.getPosition()==0 || tab.getPosition()==1 || tab.getPosition() == 2){
                                viewPAgerAdapterDailyDairy.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {

                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<DiaryResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), call.toString()+"", Toast.LENGTH_SHORT).show();
            }
        });
    }
}