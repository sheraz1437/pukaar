package com.example.pukaar.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.NotificationResponse;
import com.example.pukaar.response.Therapist_responce;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Therapist_Profile extends AppCompatActivity {
    @BindView(R.id.back_arrow_19)
    ImageView back_arrow19;
     @BindView(R.id.tp_setting)
    ImageView tp_setting;
     @BindView(R.id.therapist_name)
     TextView therapist_name;
    @BindView(R.id.therapist_type)
    TextView therapist_type;
    @BindView(R.id.my_name_is_)
    TextView introduction;
    @BindView(R.id.therapist_about)
    TextView about;
    @BindView(R.id.therapist_education)
    TextView therapist_education;
    @BindView(R.id.bottom_navigation8)
    BottomNavigationView bottomNavigationView8;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_therapist_profile);
        ButterKnife.bind(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        back_arrow19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Therapist_Profile.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
            tp_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Therapist_Profile.this ,Setting.class));
                overridePendingTransition(0,0);
            }
        });
        Call<Therapist_responce> call = apiInterface.getTherapist(CommonFunction.getToken(getApplicationContext()));
         call.enqueue(new Callback<Therapist_responce>() {
             @Override
             public void onResponse(Call<Therapist_responce> call, Response<Therapist_responce> response) {
                 if (response.body() != null) {
                     therapist_name.setText(response.body().user.getFirst_name() + " " + response.body().user.getLast_name());
                     therapist_type.setText(response.body().getType_of_doctor());
                     introduction.setText(response.body().getIntroduction());
                     about.setText(response.body().getAbout());
                     therapist_education.setText(response.body().getEducation());


                 }
             }

             @Override
             public void onFailure(Call<Therapist_responce> call, Throwable t) {

             }
         });

        bottomNavigationView8.getMenu().findItem(R.id.nav_room).setChecked(true);


        bottomNavigationView8.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        startActivity(new Intent(getApplication(), Dashboard.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_room:
                        startActivity(new Intent(getApplication(), Session_room.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.nav_profile:
                        startActivity(new Intent(getApplication(), Profile.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.bottom_settting:
                        startActivity(new Intent(getApplication(), Setting.class));
                        overridePendingTransition(0,0);
                        break;


                }
                return false;
            }
        });



    }
}