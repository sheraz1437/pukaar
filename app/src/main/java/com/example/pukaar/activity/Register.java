package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.SignUpResponse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {
    @BindView(R.id.signup_button)
    Button signup_button;

    @BindView(R.id.orientation)
    EditText firstName;

    @BindView(R.id.lName)
    EditText lastName;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.mobile)
    EditText mobile;

    @BindView(R.id.password)
    EditText password;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);





        apiInterface = APIClient.getClient().create(APIInterface.class);

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  startActivity(new Intent(Register.this ,OTP.class));
             //   overridePendingTransition(0,0);
                if(IsValid())
                {

                    Call<SignUpResponse> call = apiInterface.signUpCall(firstName.getText().toString(),lastName.getText().toString(),email.getText().toString(),mobile.getText().toString(),password.getText().toString(),password.getText().toString(),"client");
                    call.enqueue(new Callback<SignUpResponse>() {
                        @Override
                        public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                            if(response.body()!=null) {
                                CommonFunction.setToken(response.body().data.token, getApplicationContext());
                                CommonFunction.setName(response.body().data.first_name +" "+ response.body().data.last_name, getApplicationContext());
                                CommonFunction.setId(response.body().data.id.toString(), getApplicationContext());
                                CommonFunction.setEmail(response.body().data.email , getApplicationContext());
                                startActivity(new Intent(Register.this, OTP.class));
                                overridePendingTransition(0, 0);
                                // notification

                            }

                            else    {
                                Toast.makeText(getApplicationContext(),"User Already Exist ",Toast.LENGTH_SHORT).show();

                            }

                        }



                        @Override
                        public void onFailure(Call<SignUpResponse> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });


    }
    private boolean IsValid()
    {
        //if(email.getText().toString().trim().length()<5)
        if(email.getText().toString().trim().length()<5) {
            Toast.makeText(getApplicationContext(),"Please Enter valid Email Address",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(email.getText().toString()) || !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            Toast.makeText(getApplicationContext(),"Please Enter valid Email Address",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(lastName.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter Last Name",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(firstName.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(),"Please Enter First Name",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(mobile.getText().toString().trim().length() < 10) {
            Toast.makeText(getApplicationContext(),"Please Enter Valid Mobile Number",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password.getText().toString().trim().length() < 7) {
            Toast.makeText(getApplicationContext(),"Please Enter maximum 7 digit Password",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }




}