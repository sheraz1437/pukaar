package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pukaar.R;
import com.example.pukaar.adapter.Speacial_Offers_Adapters;
import com.example.pukaar.apiinterface.APIClient;
import com.example.pukaar.apiinterface.APIInterface;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.BankDetailResponse;
import com.example.pukaar.response.LoginResponse;
import com.example.pukaar.response.SpecialOfferResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Buy_session1 extends AppCompatActivity {
    @BindView(R.id.buy_now_button)
    Button buy_now_button;
    @BindView(R.id.back_arrow_1)
    ImageView back_arrow;
    @BindView(R.id.plus_button)
    ImageView plus_button;
    @BindView(R.id.counter)
    TextView counter;
    @BindView(R.id.total_amount)
    TextView total_amount;
    @BindView(R.id.per_session_price)
    TextView per_session_price;
    @BindView(R.id.minus_button)
            ImageView minus_button;
    int counter1=1;
    String value="1",value1="400";
    @BindView(R.id.offer1_Recycler)
    RecyclerView offer1_Recycler;

    @BindView(R.id.bank_name)
     TextView bank_name;
    @BindView(R.id.branch_name)
    TextView branch_name;
    @BindView(R.id.account_number)
    TextView account_number;
    @BindView(R.id.account_title)
    TextView account_title;
    @BindView(R.id.Iban)
    TextView iban;
    @BindView(R.id.buy)
    TextView buy;
    @BindView(R.id.toolbar_session_type)
    TextView toolbar_session_type;

    private APIInterface apiInterface;
    public BankDetailResponse bankDetailResponse;
    private String s ="";
    private Speacial_Offers_Adapters speacial_offers_adapters;
    private BankDetailResponse bankDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_session1);
        ButterKnife.bind(this);
       Intent intent = getIntent();
        s = intent.getStringExtra("buy");
        buy.setText(s);
        toolbar_session_type.setText(s + " Session");

        apiInterface = APIClient.getClient().create(APIInterface.class);
        plus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              increament_sessions();
            }
        });
        minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              decrement_sessions();
            }
        });



        buy_now_button.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Buy_session1.this ,Buy_session2.class);

                intent.putExtra("quantity" , value);
                intent.putExtra("total Price" , value1);
                intent.putExtra("buy" ,s );
                intent.putExtra("bank name" , bank_name.getText().toString());
                intent.putExtra("branch name" , branch_name.getText().toString());
                intent.putExtra("account number" , account_number.getText().toString());
                intent.putExtra("account title" , account_title.getText().toString());
                intent.putExtra("iban" , iban.getText().toString());

                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });


        setUpSpecialOffer();
        setUpBankDetails();
    }

    private void setUpSpecialOffer() {
        Call<ArrayList<SpecialOfferResponse>> call = apiInterface.getSpecialOffer(CommonFunction.getToken(getApplicationContext()));
        call.enqueue(new Callback<ArrayList<SpecialOfferResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<SpecialOfferResponse>> call, Response<ArrayList<SpecialOfferResponse>> response) {
                if(response.body() != null) {
                    speacial_offers_adapters = new Speacial_Offers_Adapters(getApplicationContext(), response.body(),s );
                    offer1_Recycler.setAdapter(speacial_offers_adapters);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    offer1_Recycler.setLayoutManager(linearLayoutManager);
                    if(bankDetails!= null)
                    if(speacial_offers_adapters!= null)
                    {
                        speacial_offers_adapters.setBankDetailData(bankDetails);
                    }
                }

            }

            @Override
            public void onFailure(Call<ArrayList<SpecialOfferResponse>> call, Throwable t) {
            }
        });

    }
    private void setUpBankDetails() {
        Call<ArrayList<BankDetailResponse>> call = apiInterface.getBankDetail(CommonFunction.getToken(getApplicationContext()));
        call.enqueue(new Callback<ArrayList<BankDetailResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<BankDetailResponse>> call, Response<ArrayList<BankDetailResponse>> response) {
                if(response.body() != null) {
                    if (response.body().size() >=1){
                        bank_name.setText(response.body().get(0).getBankName());
                        branch_name.setText(response.body().get(0).getBranchName());
                        account_number.setText(response.body().get(0).getAccountNumber());
                        account_title.setText(response.body().get(0).getAccountTitle());
                        iban.setText(response.body().get(0).getIban());
                        bankDetails = response.body().get(0);
                        if(speacial_offers_adapters!= null)
                        {
                            /*speacial_offers_adapters.setBankDetailData(response.body().get(0));*/
                        }
                    }

                    /*Toast.makeText(getApplicationContext(), response.body().size() + "", Toast.LENGTH_SHORT).show();*/
                }

            }

            @Override
            public void onFailure(Call<ArrayList<BankDetailResponse>> call, Throwable t) {
            }
        });
    }


    public void increament_sessions(){
        counter1++;
        value = Integer.toString(counter1);
        counter.setText(value);
        int a= Integer.valueOf(per_session_price.getText().toString());
        int b= a*counter1;
        value1= Integer.toString(b);
        total_amount.setText(value1);
    }

    public void decrement_sessions(){
        if (counter1>1){
            counter1--;}
        value = Integer.toString(counter1);
        counter.setText(value);
        int a= Integer.valueOf(per_session_price.getText().toString());
        int b= a*counter1;
        value1= Integer.toString(b);
        total_amount.setText(value1);
    }


}