package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Terms_Conditions extends AppCompatActivity {
    @BindView(R.id.back_arrow_23)
    ImageView back_arrow_23;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        ButterKnife.bind(this);
        back_arrow_23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Terms_Conditions.this,Setting.class));
                overridePendingTransition(0,0);
            }
        });
    }
}