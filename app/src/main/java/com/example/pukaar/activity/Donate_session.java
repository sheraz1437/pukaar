package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pukaar.R;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Donate_session extends AppCompatActivity {
    @BindView(R.id.donate_now_button)
    Button donate_now_button;
    @BindView(R.id.back_arrow_16)
    ImageView back_arrow16;
    @BindView(R.id.donate)
    TextView donate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate_session);
        ButterKnife.bind(this);
        // Donate


        donate_now_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Donate_session.this ,Donated_session.class));
                overridePendingTransition(0,0);
            }
        });
        back_arrow16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Donate_session.this ,Dashboard.class));
                overridePendingTransition(0,0);
            }
        });
    }
}