package com.example.pukaar.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.pukaar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Change_therapist2 extends AppCompatActivity {
    @BindView(R.id.back_arrow_18)
    ImageView back_arrow_18;
    @BindView(R.id.go_to_session_room_button1)
    Button go_to_session_room_button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_therapist2);
        ButterKnife.bind(this);
        back_arrow_18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist2.this,Change_therapist1.class));
                overridePendingTransition(0,0);
            }
        });
        go_to_session_room_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Change_therapist2.this,Session_room.class));
                overridePendingTransition(0,0);
            }
        });
    }
}