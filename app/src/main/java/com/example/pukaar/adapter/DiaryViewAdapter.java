package com.example.pukaar.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pukaar.R;
import com.example.pukaar.common.CommonFunction;
import com.example.pukaar.response.DiaryResponse;

import java.util.ArrayList;
import java.util.List;

public class DiaryViewAdapter extends RecyclerView.Adapter<DiaryViewAdapter.ViewHolder> {

    private List<DiaryResponse.FirstDatum.Datum> diaryResponseData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public DiaryViewAdapter(Context context, ArrayList<DiaryResponse.FirstDatum.Datum> data) {
        this.mInflater = LayoutInflater.from(context);
        this.diaryResponseData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.diary_recyler_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tileTextView.setText(diaryResponseData.get(position).getMood());
        holder.timeTextView.setText(CommonFunction.changeDateFormat(diaryResponseData.get(position).getCreatedAt(),true));
        holder.dateTextView.setText(CommonFunction.changeDateFormat(diaryResponseData.get(position).getCreatedAt(),false));

 if (diaryResponseData.get(position).getMood().toString().toLowerCase().equalsIgnoreCase("Crying")){
     holder.mood_emoji.setImageResource(R.drawable.crying);
 }
        if (diaryResponseData.get(position).getMood().toString().toLowerCase().equalsIgnoreCase("Depressed")){
            holder.mood_emoji.setImageResource(R.drawable.depressed);
        }
        else if (diaryResponseData.get(position).getMood().toString().toLowerCase().equalsIgnoreCase("Excited")){
            holder.mood_emoji.setImageResource(R.drawable.excited);
        }
        else if  (diaryResponseData.get(position).getMood().toString().toLowerCase().equalsIgnoreCase("OK")){
            holder.mood_emoji.setImageResource(R.drawable.ok);
        }
        else if (diaryResponseData.get(position).getMood().toString().toLowerCase().equalsIgnoreCase("Sad")){
            holder.mood_emoji.setImageResource(R.drawable.sad);
        }
        else    {
            holder.mood_emoji.setImageResource(R.drawable.ic_bad);
        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return diaryResponseData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView timeTextView,tileTextView,dateTextView;
        ImageView mood_emoji;

        ViewHolder(View itemView) {
            super(itemView);
            timeTextView = itemView.findViewById(R.id.timeTextView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            tileTextView = itemView.findViewById(R.id.titleTextView);
            mood_emoji = itemView.findViewById(R.id.mood_emoji);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    DiaryResponse.FirstDatum.Datum getItem(int position) {
        return diaryResponseData.get(position);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
