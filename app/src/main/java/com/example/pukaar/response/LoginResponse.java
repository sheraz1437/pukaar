package com.example.pukaar.response;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {


    @SerializedName("data")
    public Datum data = null;

    public class Datum {

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String[] getRole() {
            return role;
        }

        public void setRole(String[] role) {
            this.role = role;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        @SerializedName("token")
        public String token;
        @SerializedName("first_name")
        public String firstName;
        @SerializedName("last_name")
        public String LastName;
        @SerializedName("user_id")
        public Integer id;
        @SerializedName("email")
        public String email;
        @SerializedName("role")
        public String[] role;

    }
    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }
}
